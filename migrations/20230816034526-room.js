'use strict';
const { DataTypes } = require("sequelize");
/** @type {import('sequelize').QueryInterface} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('room', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      ownerId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "user_game",
          key: "userGameId",
        },
      },
      capacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 2,
      },
      currentPlayers: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      player1Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      player2Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      winner: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });

    // Define any additional alterations, indices, or constraints here
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('room');
  }
};
