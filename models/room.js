"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      room.belongsTo(models.user_game, { as: "owner", foreignKey: "ownerId" });
      room.belongsToMany(models.user_game, {
        through: "UserRoom",
        as: "participants",
        foreignKey: "roomId",
      });
    }
  }
  room.init(
    {
      ownerId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "user_game",
          key: "id",
        },
      },
      capacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 2,
      },
      currentPlayers: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      player1Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      player2Choice: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      winner: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "room",
      tableName: "room",
    }
  );
  return room;
};
