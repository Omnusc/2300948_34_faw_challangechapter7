'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    static associate(models) {
      user_game_history.belongsTo(models.user_game, {
        foreignKey: 'userGameId',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      });
    }
  }
  user_game_history.init({
    userGameHistoryId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userGameId: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    uuid: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
    },
    win: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    loss: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    timePlayhrs: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'user_game_history',
    modelName: 'user_game_history',
  });

  return user_game_history;
};
