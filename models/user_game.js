'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
       // for user_game_bio
      // one to one relation, one id one bio
      user_game.hasOne(models.user_game_bios,{
        foreignKey: 'userGameId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
      
      // for user_game_history
      // one to many, one player (id) can have multiple history
      user_game.hasMany(models.user_game_history,{
        foreignKey: 'userGameId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  user_game.init({
    userGameId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    uuid: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
    },
    userGameUsername: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userGamePassword: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userGameRole: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    tableName: 'user_game',
    modelName: 'user_game',
  });
  return user_game;
};