const express = require("express");
const app = express();
// Middleware to get user input
const bodyParser = require("body-parser");
const passport = require("passport");
const session = require("express-session");



const aboutusRoutes = require("./routes/aboutus");
const workRoutes = require("./routes/work");
const contactRoutes = require("./routes/contact");
const signRoutes = require("./routes/sign");
const playRoutes = require("./routes/play");
const loginRoutes = require("./routes/login")
const roomRoutes = require("./routes/room");

let login = require("./public/login.json");
const { sequelize, user_game, user_game_history, user_game_bios } = require("./models");

app.set("view engine", "ejs");
app.use(express.json());

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));

// app.get("/", (req, res) =>
// {
//     res.sendFile(__dirname + "/views/index.ejs");
// });
app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: true
  }));



app.get("/", (req, res)=>
{
    res.render("index", {loginText: "Login"});
});


app.use(aboutusRoutes);
app.use(workRoutes);
app.use(contactRoutes);
app.use(signRoutes);
app.use(playRoutes);
app.use(passport.initialize());
app.use(passport.session());
app.use(loginRoutes);
app.use(roomRoutes);

app.get("/dashboard", (req, res) => {
    if (req.isAuthenticated()) {
      user_game.findAll().then((users) => {
        user_game_bios.findAll().then((usersBios) => {
          user_game_history.findAll().then((usersHistory) => {
            res.render("dashboard", { users, usersBios, usersHistory });
          });
        });
      });
    } else {
      // If not authenticated, redirect to login
      res.redirect("/login");
    }
  });
  
// for creating user
// page for create
app.get("/create", (req, res)=>
{
    res.render("create", {warning: ""});
});

// the data creation logic in dashboard
app.post("/users", (req, res)=>{
    if(req.body.password != req.body.repassword)
    {
        res.render("create", {warning: "Password Does Not Match"});
    }
    else
    user_game.create({
        userGameUsername: req.body.username,
        userGamePassword: req.body.password,
        userGameRole: req.body.role
    }).then((newUserGame) => {
        return user_game_bios.create({
            userGameId: newUserGame.userGameId,
            name: req.body.name,
            email: req.body.email,
            gender: req.body.gender
        }).then((newUserBios) => {
            return user_game_history.create({
                userGameId: newUserBios.userGameId,
                win: req.body.win,
                loss: req.body.loss,
                timePlayhrs: req.body.hrsplay
            })
        }).then(() =>
        {
            console.log("User Created");
            res.redirect("/dashboard");
        })
    })
});

// for deleting logic
app.post("/delete/:userGameId" , (req, res) =>
{
    // need to get the userGameId as a variable
    // because it will be destoryed and we need the value
    const userGameIdConst = req.params.userGameId;
    user_game.destroy({
        where: {userGameId: userGameIdConst}
    }).then(() => 
    {
        return user_game_bios.destroy({
            where: {userGameId: userGameIdConst}
        }).then(() => 
        {
            return user_game_history.destroy({
                where: {userGameId: userGameIdConst}
            }).then(()=>{
                console.log("User Deleted")
                res.redirect("/dashboard");
            })
        })
    })
});


// for updating
// page for update (use post so we can pass the id value)
app.post("/updateForm/:userGameId", async (req, res) => 
{
    const userGameIdUrl = req.params.userGameId;

    user_game.findOne({
        where: {userGameId: userGameIdUrl}
    }).then((userGame) => {
        user_game_history.findOne({
            where: {userGameId: userGame.userGameId}
        }).then((userGameHistory) => {
            user_game_bios.findOne({
                where:{userGameId: userGameHistory.userGameId}
            }).then((userGameBios)=>{
                res.render("updateForm", {userGame, userGameHistory, userGameBios, warning: ""});
            })
        })
    })
});

app.post("/update/:userGameId", async (req, res)=>
{
    const userGameIdUrl = req.params.userGameId;
    if(req.body.passwordUpdate != req.body.repassword)
    {
        console.log("Password doesnt match")
    }
    else
    user_game.update({
        userGameUsername: req.body.usernameUpdate,
        userGamePassword: req.body.passwordUpdate,
        userGameRole: req.body.role
    },
    {
        where: {userGameId: userGameIdUrl},
    }
    ).then(() => {
        user_game_bios.update(
        {
            name: req.body.nameUpdate,
            email: req.body.emailUpdate,
            gender: req.body.genderUpdate,
        },
        {
            where: {userGameId: userGameIdUrl}
        }).then(() => {
            user_game_history.update(
            {
                win: req.body.winUpdate,
                loss: req.body.lossUpdate,
                timePlayhrs: req.body.hrsplayUpdate,
            },
            {
                where: {userGameId: userGameIdUrl}
            }).then(()=>
            {
                console.log("User Updated")
                // res.render("Dashboard", user)
            })

        })
    })
});


    // const userGame = await user_game.findOne({
    //     where: { userGameId },
    //     include: [user_game_bios],
    // })
    // idk why this is working
    // im just writing shit up, but
    // only allah know why this is working
    // const history = await user_game_history.findOne({
    //     where: {userGameId},
    //     include: [user_game]
    // })
    // pass the id to the new page
    // res.render("updateForm", {test, history, userGame, userGameId, warning: ""});

// updating
// app.post("/update/userGameId", (req, res) => 
// {

// })



// For login GET
app.get("/login", (req, res) => {
    res.render("login", {errorText: ""});
});

// Show all

// app.get("/dashboard", (req, res) => {
//     user_game.findAll()
//     .then((users) => {
//         user_game_bios.findAll()
//         .then((usersBios) => {
//             user_game_history.findAll().then((usersHistory)=>{
//                 res.render("./dashboard", { users, usersBios, usersHistory});
//             })
//         })
        
//     })
// });


// delete data


// When user post data
// app.post("/login", (req, res)=>
// {
//     // req.body. (point at "name" tag)
//     let email = req.body.email;
//     let password = req.body.password;
//     // login is the json
//     if (email === login.email && password === login.password) 
//     {
//         res.redirect("/dashboard");
//     }
    
//     else  
//     {
//         res.render("login", { errorText: "Invalid Email or Password" });
//     }
      
// });




// app.get("/dashboard", async (req, res) => {
//     try {
//       const { userGameId } = req.params;
//       // find one, where the userGameId is the same as the url "/user_game/1" means id 1
//       const allUserGame = await user_game.findAll();
//       // if 
//       if (allUserGame.length> 0) {
//         res.render("user_game", { userGames: userGame });
//       } else {
//         res.status(404).json({ error: "User Game not found" });
//       }
//     } catch (err) {
//       console.error("Error fetching user game:", err);
//       res.status(500).json({ error: "An error occurred" });
//     }
//   });


  

app.listen(3000, async () => {
    console.log('Server is running on port 3000');
    await sequelize.authenticate();
});

// async function main(){
//     await sequelize.sync({force:true});
// }
// main();

