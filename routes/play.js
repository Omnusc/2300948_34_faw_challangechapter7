const express = require("express");
const router = express.Router();
const play = require("../controllers/play");

// read
router.get("/play", async(req, res) => 
{
    const player1Choice = req.query.choice;
    await play.getPlay(req, res, player1Choice);
    play.getLogic(req, res, player1Choice);
})
module.exports = router;
