const express = require("express");
const router = express.Router();
const aboutus = require("../controllers/aboutus");

// read
router.get("/about-us", aboutus.getAboutus);
module.exports = router;
