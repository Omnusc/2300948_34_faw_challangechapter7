const express = require("express");
const router = express.Router();
const loginController = require("../controllers/login");
const passport = require("../lib/passport");

router.get("/login", loginController.getLogin,);
router.post('/login', (req, res, next) => {
    console.log("POST /login route reached");
    passport.authenticate('local', {
      successRedirect: '/checkRole',
      failureRedirect: '/login',
    })(req, res, next);
  });
router.get("/checkRole", loginController.checkRole);

module.exports = router;