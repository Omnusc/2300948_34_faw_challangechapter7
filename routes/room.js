const express = require("express");
const router = express.Router();
const passport = require("passport");
const roomController = require("../controllers/room");
const room = require("../models").room;

// Route for creating a room
router.post(
  "/create-room",
//   passport.authenticate("jwt", { session: false }), // Use { session: false } to disable session-based authentication
// stuck authorization
// Hard code owner id
// Sad...
  roomController.createRoom
);
// Route to display a list of available rooms
router.get("/room", async (req, res) => {
    try {
        const rooms = await room.findAll();
        res.render("room", { rooms });
    } catch (error) {
        console.error("Error fetching rooms:", error);
        res.status(500).json({ error: "An error occurred" });
    }
});



// Route to join a specific room
router.get("/play/:roomId", (req, res) => {
    const roomId = req.params.roomId;

    // Fetch room details from the database based on the roomId
    // You can use the `roomId` to retrieve the room details

    // Assuming you have a method to retrieve room details by ID 
    
    res.render("play", { roomId }); 
});

module.exports = router;


module.exports = router;

