const express = require("express");
const passport = require("../lib/passport");
const router = express.Router();
const sign = require("../controllers/sign");

// read "the page"
router.get("/sign", sign.getSign);

// create data
router.post("/sign", sign.postSignup);
module.exports = router;