const passport = require("../lib/passport");
const jwt = require('jsonwebtoken'); 

const getLogin = (req, res) => {
  res.render("login", { errorText: "" });
};

const postLogin = (req, res, next) => { 
  passport.authenticate("local", (err, user, info) => { 
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.render("login", { errorText: "Invalid credentials" });
    }
    req.login(user, { session: false }, (err) => { 
      if (err) {
        return next(err);
      }
      // Token
      const token = jwt.sign({ id: user.userGameId }, 'secret'); 
      res.cookie('jwt', token); 
      return res.redirect("/checkRole");
    });
  })(req, res, next);
};

const checkRole = (req, res) => {
  if (req.isAuthenticated()) {
    if (req.user.userGameRole === "superadmin") {
        console.log(req.user.userGameRole);
        console.log("1st if");
      res.redirect("/dashboard"); // Admin goes to dashboard
    } else {
      res.redirect("/"); // Non-admin goes to home
      console.log(req.user.userGameRole);
      console.log("2nd if");
    }
  } else {
    // if something wrong back to login
    res.redirect("/login");
    console.log(req.user.userGameRole);
    console.log("else");
  }
};

exports.getLogin = getLogin;
exports.postLogin = postLogin;
exports.checkRole = checkRole;
