const { room } = require("../models");

exports.createRoom = (req, res) => {
    const ownerId = req.user.id; // Assuming that the authenticated user ID is accessible via req.user.id

    console.log("Decoded user ID:", ownerId); // Add this console log to check the extracted ownerId

    room.create({
        // Hard code owner id
        // Sad...
        // Stuck
        ownerId: 12,
    })
    .then((newRoom) => {
        console.log("Room created with id:", newRoom.id);
        res.json({ roomId: newRoom.id });
    })
    .catch((error) => {
        console.error("Error creating room:", error);
        res.status(500).json({ error: "Failed to create room" });
    });
};

// redirect room

