// byrcrypt
const bcrypt = require('bcrypt');
// get model;
const { sequelize, user_game, user_game_history, user_game_bios } = require("../models");


const getSign = (req, res) => {
    res.render("sign");
  };
  
  // EXPORT ALL THE FUNCTIONS


const postSignup = async (req, res) =>
{
    try{
        // the curly brace must be the same as in ejs
        const { username, password, role, name, email, gender } = req.body;
        const hashedPassword = await bcrypt.hash(password, 10);
        // create user with hashed password
        const newUserGame = await user_game.create({
            userGameUsername: username,
            userGamePassword: hashedPassword,
            userGameRole: role
        });

        const newUserBios = await user_game_bios.create({
            userGameId: newUserGame.userGameId,
            name: name,
            email: email,
            gender: gender
        });

        await user_game_history.create({
            userGameId: newUserGame.userGameId,
            win: 0,
            loss: 0,
            timePlayhrs: 0,
        });
        console.log("User Created");
        res.redirect("/");
        // .then((newUserGame) => {
        //     return user_game_bios.create({
        //         userGameId: newUserGame.userGameId,
        //         name: name,
        //         email: email,
        //         gender: gender
        //     }).then((newUserBios) => {
        //         return user_game_history.create({
        //             userGameId: newUserBios.userGameId,
        //             win: 0,
        //             loss: 0,
        //             timePlayhrs: 0,
        //         })
        //     }).then(() =>
        //     {
        //         console.log("User Created");
        //     })
        // });
    }catch(error)
    {
        console.log("Error creating user", error);
        res.redirect("/sign");
    }
}

exports.postSignup = postSignup;
exports.getSign = getSign;