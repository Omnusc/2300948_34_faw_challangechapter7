const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const bcrypt = require("bcrypt");
const { user_game } = require("../models");

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret', 
};

// Local Strategy
passport.use(
  new LocalStrategy(async (username, password, done) => {
    try {
      const user = await user_game.findOne({ where: { userGameUsername: username } });
      if (!user) {
        return done(null, false, { message: "User not found" });
      }

      const passwordMatch = await bcrypt.compare(password, user.userGamePassword);
      if (!passwordMatch) {
        return done(null, false, { message: "Invalid password" });
      }

      return done(null, user);
    } catch (err) {
      return done(err);
    }
  })
);

// JWT Strategy
passport.use(
  new JwtStrategy(jwtOptions, async (payload, done) => {
    try {
      const user = await user_game.findByPk(payload.userGameId);
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    } catch (err) {
      done(err, false);
    }
  })
);



passport.serializeUser((user, done) => {
  done(null, user.userGameId);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await user_game.findByPk(id);
    done(null, user);
  } catch (err) {
    done(err);
  }
});

module.exports = passport;
